# Contributing

To contribute to this project, follow these steps:

1. Install the development dependencies: `pip install -r requirements.txt`.
2. Install pre-commit hooks: `pre-commit install`.
3. Before committing, run the pre-commit hooks: `pre-commit`.
4. If the pre-commit hooks fail, fix the issues and try again.
